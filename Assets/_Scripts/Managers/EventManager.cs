using UnityEngine.Events;

/// <summary>
/// Handles global-level events
/// </summary>
public static class EventManager
{
    public static UnityEvent<int, string> CallForAnimation = new UnityEvent<int, string>();
}
