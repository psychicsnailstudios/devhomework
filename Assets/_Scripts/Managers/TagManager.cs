/// <summary>
/// Stores any id information for easy recall and reference
/// </summary>
public static class TagManager
{
    public struct AnimationIds
    {
        public static readonly string[] Idle = { "Idle03", "Idle03 0", "Idle03 1", "Idle03 3" };
        public const string Run = "Run01FWD";
    }
}
