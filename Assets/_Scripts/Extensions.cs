﻿using System.Collections.Generic;

public static class Extensions
{
    /// <summary>
    /// Gets a random object from the array
    /// </summary>
    public static T GetRandom<T>(this T[] list)
    {
        return list[UnityEngine.Random.Range(0, list.Length)];
    }
}