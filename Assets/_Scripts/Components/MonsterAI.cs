using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Monster))]
[RequireComponent(typeof(NavMeshAgent))]
public class MonsterAI : MonoBehaviour
{
    #region Fields

    // settings
    public float minIdle = 1f;
    public float maxIdle = 5f;
    public Transform[] idlePoints;

    // var refs
    Transform _currentIdlePoint;
    float _time;

    // component refs
    NavMeshAgent _nav;
    Monster _monster;

    #endregion

    #region Unity Methods

    void Start()
    {
        _nav = GetComponent<NavMeshAgent>();
        _monster = GetComponent<Monster>();

        _time = 3f;
    }

    void Update()
    {
        switch (_monster.state)
        {
            case MonsterState.Idle:
                Idle();
                break;

            case MonsterState.Moving:
                Moving();
                break;
        }
    }

    #endregion

    #region Methods

    void Idle()
    {
        // decrement time
        _time -= Time.deltaTime;

        // start moving once idle is done
        if (_time <= 0)
        {
            // pick new point
            _currentIdlePoint = idlePoints.GetRandom();

            // set new destination
            _nav.isStopped = false;
            _nav.SetDestination(_currentIdlePoint.position);

            // change state
            _monster.state = MonsterState.Moving;
        }
    }

    void Moving()
    {
        // idle once point is reached
        if (_nav.remainingDistance <= 0.1f)
        {
            // set idle time
            _time = Random.Range(minIdle, maxIdle);

            // set nav
            _nav.isStopped = true;

            // change state
            _monster.state = MonsterState.Idle;
        }
    }

    #endregion
}
