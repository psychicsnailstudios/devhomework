using UnityEngine;

public class FollowCam : MonoBehaviour
{
    #region Fields

    public Transform target;
    public Transform rig;
    public bool lookAt = true;
    public float rotateSpeed = 50;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    #endregion

    #region Methods

    void Update()
    {
        Vector3 desiredPosition = target.position;
        Vector3 smoothedPosition = Vector3.Lerp(rig.position, desiredPosition, smoothSpeed);
        rig.position = smoothedPosition;

        if (lookAt)
        {
            transform.LookAt(target);
        }
    }

    public void RotateCam(bool right = true)
    {
        if (right)
        {
            rig.Rotate(0, rotateSpeed * Time.deltaTime, 0);
        }
        else
        {
            rig.Rotate(0, -rotateSpeed * Time.deltaTime, 0);
        }
    }

	#endregion
}
