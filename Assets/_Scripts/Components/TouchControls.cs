using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Monster))]
[RequireComponent(typeof(NavMeshAgent))]
public class TouchControls : MonoBehaviour
{
    #region Fields

    public Camera cam;
    public FollowCam fCam;
    public float tapRange;
    public float swipeRange;

    Vector2 _startTouchPosition;
    Vector2 _currentTouchPosition;
    Vector2 _endTouchPosition;

    NavMeshAgent _nav;
    Monster _monster;

    #endregion

    #region Unity Methods

    void Start()
    {
        _nav = GetComponent<NavMeshAgent>();
        _monster = GetComponent<Monster>();
    }

    void Update()
    {
        // idle once point is reached
        if (_nav.remainingDistance <= 0.1f)
        {
            _monster.state = MonsterState.Idle;
        }

        // get touch input
        if (Input.touchCount > 0 && !IsPointerOverUIObject())
        {
            GetTouch();
        }
    }

    #endregion

    #region Methods

    void GetTouch()
    {
        Touch touch = Input.GetTouch(0);

        switch (touch.phase)
        {
            case TouchPhase.Began:
                _startTouchPosition = touch.position;
                break;

            case TouchPhase.Moved:
                _currentTouchPosition = touch.position;
                Vector2 moveDistance = _currentTouchPosition - _startTouchPosition;

                // left swipe
                if (moveDistance.x < -swipeRange)
                {
                    fCam.RotateCam(false);
                }
                // right swipe
                else if (moveDistance.x > swipeRange)
                {
                    fCam.RotateCam(true);
                }
                break;

            case TouchPhase.Ended:
                _endTouchPosition = touch.position;

                Vector2 dragDistance = _endTouchPosition - _startTouchPosition;

                if (Mathf.Abs(dragDistance.x) < tapRange && Mathf.Abs(dragDistance.y) < tapRange)
                {
                    GoToTouch();
                }
                break;
        }
    }

    void GoToTouch()
    {
        Ray ray = cam.ScreenPointToRay(Input.GetTouch(0).position);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            _monster.state = MonsterState.Moving;
            _nav.SetDestination(hit.point);
        }
    }

    bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);

        return results.Count > 0;
    }

    #endregion
}
