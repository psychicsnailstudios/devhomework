using UnityEngine;

[RequireComponent(typeof(Monster))]
[RequireComponent(typeof(Animator))]
public class MonsterAnimator : MonoBehaviour
{
    #region Fields

    MonsterState _lastState = MonsterState.Idle;

    Animator _anim;
    Monster _monster;

    #endregion

    #region Unity Methods

    void Start()
    {
        _anim = GetComponent<Animator>();
        _monster = GetComponent<Monster>();

        EventManager.CallForAnimation.AddListener(PlayAnimation);
    }

    void Update()
    {
        if (_monster.state != _lastState)
        {
            switch (_monster.state)
            {
                case MonsterState.Idle:
                    _anim.CrossFade(TagManager.AnimationIds.Idle.GetRandom(), 0.15f);
                    break;

                case MonsterState.Moving:
                    _anim.CrossFade(TagManager.AnimationIds.Run, 0.15f);
                    break;
            }

            _lastState = _monster.state;
        }
    }

    #endregion

    #region Methods

    void PlayAnimation(int id, string animation)
    {
        if (id == 0 || _monster.id == id)
        {
            _anim.CrossFade(animation, 0.15f);
        }
    }

    #endregion
}
