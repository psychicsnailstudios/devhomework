using UnityEngine;

public class EventButton : MonoBehaviour
{
    public string animationID;

    [Tooltip("0 = both, 1 = blue, 2 = yellow")]
    public int monsterID = 0;

    public void CallAnimation()
    {
        EventManager.CallForAnimation.Invoke(monsterID, animationID);
    }
}
